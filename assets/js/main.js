(function ($) {
  $(document).ready(function () {
    $(".info-elemento").click(function () {
      $(".section-info").show();
      $("html,body").animate(
        { scrollTop: $("#section-interaccion").offset().top },
        1000
      );
      $(".texto-info").html($(this).attr("data-info"));
    });

    $(".section-info").hide();
    $(".close-icon").click(function () {
      $(".section-info").hide();
    });

    function scrollX(val) {
      $("html,body").animate({ scrollTop: $(val).offset().top }, 1000);
    }

    $(".click-scroll").click(function () {
      var tag = $(this).attr("href");
      scrollX(tag);
    });

    $("#form-contacto").submit(function (e) {
      e.preventDefault();

      var formElem = $(this).serialize();

      $.ajax({
        url: "https://prueba.antojitos-mexicanos.vip/api/v1/m",
        data: formElem,
        processData: false,
        type: "POST",
        beforeSend: function () {
          $("#btn-enviar").prop("disabled", true);
          M.Toast.dismissAll();
          M.toast({ html: "Espere un momento...", classes: "rounded blue" });
        },
        error: function () {
          M.toast({ html: "Error vuelva a intentar", classes: "rounded red" });
          $("#btn-enviar").prop("disabled", false);
        },
        success: function (data) {
          setTimeout(function () {
            M.Toast.dismissAll();
          }, 1000);

          setTimeout(function () {
            M.toast({
              html: "Informacion enviada",
              classes: "rounded green",
              inDuration: 1000,
              displayLength: 10000,
            });
            $("#form-contacto")[0].reset();
            $("#btn-enviar").prop("disabled", false);
          }, 1500);
        },
      });
    });
  });
})(jQuery);
