document.addEventListener("DOMContentLoaded", function () {
  // Parallax
  var elems = document.querySelectorAll(".parallax");
  var instances = M.Parallax.init(elems, { responsiveThreshold: 0 });
  // Carousel
  var elems1 = document.querySelectorAll(".carousel");
  var instances1 = M.Carousel.init(elems1, {
    duration: 300,
    indicators: true,
    fullWidth: true,
    dist: 0,
    shift: 0,
    padding: 0,
    numVisible: 6,
    noWrap: true,
  });
  var carousel = M.Carousel.getInstance(elems1[0]);
  setInterval(() => {
    carousel.next();
  }, 6000);
  // Menu
  var sidenavs = document.querySelectorAll(".sidenav");
  var instances = M.Sidenav.init(sidenavs);
});
